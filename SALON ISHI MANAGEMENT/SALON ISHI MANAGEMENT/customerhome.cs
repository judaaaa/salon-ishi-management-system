﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SALON_ISHI_MANAGEMENT.view_Section;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class customerhome : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        public customerhome()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {
                   
                    SqlCommand cmd = new SqlCommand("INSERT into CustomerD VALUES (@cname , @ctelnumber, @cservice , @csname ,@camount)", con);
                    cmd.CommandType = CommandType.Text;

                

                    cmd.Parameters.AddWithValue("@cname", cname.Text);
                    cmd.Parameters.AddWithValue("@ctelnumber", ctelnumber.Text);
                    cmd.Parameters.AddWithValue("@cservice", cservice.Text);
                    cmd.Parameters.AddWithValue("@csname", csname.Text);
                    cmd.Parameters.AddWithValue("@camount", camount.Text);


                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);
                    resettextfeilds();

                }
                catch (InvalidCastException exception) {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool IsValid()
        {
            if(cname.Text== String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            new Home().Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new CustomerView().Show();
            this.Hide();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }

        private void resettextfeilds()
        {
            cname.Clear();
            ctelnumber.Clear();
            cservice.SelectedIndex = -1;
            csname.Clear();
            camount.Clear();

            cname.Focus();
        }

        private void customerhome_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'salonishiDataSet.ServiceType' table. You can move, or remove it, as needed.
            this.serviceTypeTableAdapter.Fill(this.salonishiDataSet.ServiceType);

        }

        private void cservice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
          
        }
        public void CserviceCombobox()
        {
            cservice.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select servicetype from ServiceType";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                cservice.Items.Add(dr["servicetype"].ToString());
            }
            con.Close();
        }

        private void cservice_MouseClick(object sender, MouseEventArgs e)
        {
            CserviceCombobox();
        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }
    }
}
