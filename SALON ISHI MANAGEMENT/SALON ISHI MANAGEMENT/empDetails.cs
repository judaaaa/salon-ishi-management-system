﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class empDetails : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        public empDetails()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            new Home().Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new employeehome().Show();
            this.Hide();
        }

        private void empDetails_Load(object sender, EventArgs e)
        {
            GetEmpDeatils();
        }

        private void GetEmpDeatils()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("Select * from EmployeeD", con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                EmpdataGridView1.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {

             
                    con.Open();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "insert into EmployeeD values ('"+ Employee_ID.Text + "','" + Employee_Name.Text + "','" + Employee_Contact_No.Text + "','" + Employee_Address.Text + "','" + Employee_Nic.Text + "','" + Employee_Gender.Text + "','" + Employee_Type.Text + "','" + date.Value.ToString() + "','" + Employee_Email.Text + "')";
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    GetEmpDeatils();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void resettextfeilds()
        {
              Employee_ID.Clear();
              Employee_Name.Clear();
              Employee_Contact_No.Clear();
              Employee_Address.Clear();
              Employee_Nic.Clear();
              Employee_Gender.SelectedIndex = -1;
              Employee_Type.SelectedIndex = -1;
              date.Format = DateTimePickerFormat.Short;
              Employee_Email.Clear();

              Employee_ID.Focus(); 
        }

        private bool IsValid()
        {
            if (Employee_ID.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void Employee_Gender_MouseClick(object sender, MouseEventArgs e)
        {
            Gendercombobox();
        }
        private void Gendercombobox()
        {
            Employee_Gender.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select Type from Gender";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Employee_Gender.Items.Add(dr["Type"].ToString());
            }
            con.Close();
        }

        private void Employee_Type_MouseClick(object sender, MouseEventArgs e)
        {
            EmpTypecombobox();

        }
        private void EmpTypecombobox()
        {
            Employee_Type.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select Type from EmployeeType ";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Employee_Type.Items.Add(dr["Type"].ToString());
            }
            con.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }
    }
}
