﻿using SALON_ISHI_MANAGEMENT.adminfunctions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class dashboard : Form
    {
        public dashboard()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new UserManagement().Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new employeehome().Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new customerhome().Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new SuppliarManagement().Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new UserType().Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new ServiceType().Show();
            this.Hide();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            new EmployeeType().Show();
            this.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            new Gender().Show();
            this.Hide();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            new stockManagement().Show();
            this.Hide();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            new ProductType().Show();
            this.Hide();
        }
    }
}
