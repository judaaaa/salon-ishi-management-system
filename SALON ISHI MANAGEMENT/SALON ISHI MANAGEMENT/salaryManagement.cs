﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class salaryManagement : Form

    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        public salaryManagement()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            new employeehome().Show();
            this.Hide();
        }

        private void Employee_Details_MouseClick(object sender, MouseEventArgs e)
        {
            empDetailscombobox();
        }

        private void empDetailscombobox()
        {
            Employee_Details.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " Select Employee_ID , Employee_Name , Employee_Type from EmployeeD";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Employee_Details.Items.Add(dr["Employee_ID"]+"|"+dr["Employee_Name"] +"|"+dr["Employee_Type"].ToString());
                
            }
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (IsValid())
            {
                try
                {


                    con.Open();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "insert into Salary values ('" + Employee_Details.Text + "','" + Date_of_Entry.Value.ToString() + "','" + Salary_Amount.Text + "')";
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    Getsalarydetails();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void resettextfeilds()
        {
            Employee_Details.SelectedIndex = -1;
            Date_of_Entry.Format = DateTimePickerFormat.Short;
            Salary_Amount.Clear();
        }

        private bool IsValid()
        {
            if (Employee_Details.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if(Salary_Amount.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (Date_of_Entry.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void salaryManagement_Load(object sender, EventArgs e)
        {
            Getsalarydetails();
        }

        private void Getsalarydetails()
        {
                try
                {
                    SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                    SqlCommand cmd = new SqlCommand("Select * from Salary", con);
                    DataTable dt = new DataTable();

                    con.Open();

                    SqlDataReader sdr = cmd.ExecuteReader();
                    dt.Load(sdr);
                    con.Close();

                    SalarydataGridView.DataSource = dt;
                }
                catch (InvalidOperationException exc)
                {
                    MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }
    }

}
