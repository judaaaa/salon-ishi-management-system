﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SALON_ISHI_MANAGEMENT
{
    public partial class UserManagement : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        DataTable dt = new DataTable();
        
        public UserManagement()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new dashboard().Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("INSERT into UserManagement VALUES (@uid , @uname, @utype, @password, @cpassword)", con);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@uid", uid.Text);
                    cmd.Parameters.AddWithValue("@uname", uname.Text);
                    cmd.Parameters.AddWithValue("@utype", utype.Text);
                    cmd.Parameters.AddWithValue("@password", password.Text);
                    cmd.Parameters.AddWithValue("@cpassword", cpassword.Text);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    GetUserdata();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void resettextfeilds()
        {
            uid.Clear();
            uname.Clear();
            utype.SelectedIndex = -1;  
            password.Clear();
            cpassword.Clear();
        }

        private void GetUserdata()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("Select * from UserManagement", con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                userdataGridView.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool IsValid()
        {
            if (uid.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
           
            return true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
         
        }

        private void utypecombobox()
        {
            utype.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select Type from UserType";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                utype.Items.Add(dr["Type"].ToString());
            }
            con.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void UserManagement_Load(object sender, EventArgs e)
        {
            GetUserdata();
          


        }

       

        private void utype_MouseClick(object sender, MouseEventArgs e)
        {
            utypecombobox();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            

            uid.Clear();
            uname.Clear();
            utype.SelectedIndex = -1;
            password.Clear();
            cpassword.Clear();
            //resetting the textebox color and the lable//
            Resettbcolor(uid,u1);


        }

        private void utype_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void UserManagement_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void uid_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validation
            RegexExpression(@"^([\w]+)$", uid, u1, "User ID : ");
        }
        public void RegexExpression(String reg, TextBox tb,Label l,String s) {

            Regex regex = new Regex(reg);
            if (regex.IsMatch(tb.Text))
            {
                tb.BackColor = Color.White;
                
                //reseting the BackColor of the TextBox if it is Emply//
                TextboxisEmpty(uid, u1);
            }
            else
            {
                tb.BackColor = Color.Red;
                l.ForeColor = Color.Red;
                l.Text = s + "Invalid";

                //reseting the BackColor of the TextBox if it is Emply//
                TextboxisEmpty(uid, u1);
            }
          
        }

        public void Resettbcolor(TextBox tb,Label l)
        {
            tb.BackColor = Color.White;
            l.Text = "";
        }
        public void TextboxisEmpty(TextBox tb,Label l)
        {
            if (tb.Text == "")
            {
                tb.BackColor = Color.White;
                l.Text ="";
            }
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
           // SqlCommand cmd = new SqlCommand("Select * from UserManagement where uid = " +uid.Text, con);
           // DataTable dt = new DataTable();
           // SqlDataAdapter da = new SqlDataAdapter(cmd);
           // da.Fill(dt);
           // userdataGridView.DataSource = dt;
        }

        private void Searchbar_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void userdataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            uid.Text = userdataGridView.SelectedRows[0].Cells[0].Value.ToString();
            uname.Text = userdataGridView.SelectedRows[0].Cells[1].Value.ToString();
            utype.Text = userdataGridView.SelectedRows[0].Cells[2].Value.ToString();
            password.Text = userdataGridView.SelectedRows[0].Cells[3].Value.ToString();
            cpassword.Text = userdataGridView.SelectedRows[0].Cells[4].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (uid.Text != "") {
                    SqlCommand cmd = new SqlCommand("UPDATE UserManagement SET uname = @uname,utype = @utype, password = @password, cpassword = @cpassword where uid = @uid", con);
                    cmd.CommandType = CommandType.Text;


                    cmd.Parameters.AddWithValue("@uname", uname.Text);
                    cmd.Parameters.AddWithValue("@utype", utype.Text);
                    cmd.Parameters.AddWithValue("@password", password.Text);
                    cmd.Parameters.AddWithValue("@cpassword", cpassword.Text);
                    cmd.Parameters.AddWithValue("@uid", this.uid);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Updated Successfuly", "Updated", MessageBoxButtons.OK, MessageBoxIcon.None);

                    GetUserdata();
                    resettextfeilds();
                }
                else
                {
                    MessageBox.Show("Please Select the Cell from the Table to Update", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
            catch(InvalidOperationException exce)
            {
                MessageBox.Show("" + exce, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
