﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class stockManagement : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");

        public stockManagement()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            new SuppliarManagement().Show();
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new Home().Show();
            this.Hide();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void Supplair_Details_MouseClick(object sender, MouseEventArgs e)
        {
            getsupdeatils();
        }

        private void getsupdeatils()
        {
            Supplair_Details.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = " Select Supplair_Id , Supplair_Name  from Supplair";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Supplair_Details.Items.Add(dr["Supplair_Id"] + "  |  " + dr["Supplair_Name"].ToString());

            }
            con.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {


                    con.Open();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "insert into Stock values ('" + Supplair_Details.Text + "','" + product_Type.Text + "','" + qty.Text + "','" + amount_per_item.Text + "','" + date.Value.ToString() + "','" + Total_Amount.Text + "')";
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    GetStockdetails();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private bool IsValid()
        {
            if (Supplair_Details.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (product_Type.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (qty.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (amount_per_item.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (date.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (Total_Amount.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void resettextfeilds()
        {
            Supplair_Details.SelectedIndex = -1;
            product_Type.SelectedIndex = -1;
            qty.Clear();
            amount_per_item.Clear();
            date.Format = DateTimePickerFormat.Short;
            Total_Amount.Clear();
        }

        private void GetStockdetails()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("Select * from Stock", con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                StockdataGridView.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void button14_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }

        private void stockManagement_Load(object sender, EventArgs e)
        {
            GetStockdetails();
        }

        private void product_Type_MouseClick(object sender, MouseEventArgs e)
        {
            GetProducttype();
        }

        private void GetProducttype()
        {
            product_Type.Items.Clear();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Select Type from ProductType ";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                product_Type.Items.Add(dr["Type"].ToString());
            }
            con.Close();
        }
    }
}
