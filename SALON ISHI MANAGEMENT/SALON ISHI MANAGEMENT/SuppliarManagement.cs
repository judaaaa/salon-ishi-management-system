﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class SuppliarManagement : Form
    {
        public SuppliarManagement()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new supDetails().Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new stockManagement().Show();
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new Home().Show();
            this.Hide();
        }
    }
}
