﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT
{
    public partial class supDetails : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        public supDetails()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new Home().Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            new SuppliarManagement().Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private void Getsupdata()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("Select * from Supplair", con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                supdataGridView1.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("INSERT into Supplair VALUES (@Supplair_Id , @Supplair_Name, @Supplair_Contact_No, @Supplair_Address, @Supplair_Email)", con);
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Supplair_Id", Supplair_Id.Text);
                    cmd.Parameters.AddWithValue("@Supplair_Name", Supplair_Name.Text);
                    cmd.Parameters.AddWithValue("@Supplair_Contact_No", Supplair_Contact_No.Text);
                    cmd.Parameters.AddWithValue("@Supplair_Address", Supplair_Address.Text);
                    cmd.Parameters.AddWithValue("@Supplair_Email", Supplair_Email.Text);
                   
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    Getsupdata();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                catch(SqlException exce)
                {
                    MessageBox.Show("Invalid ID entering", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool IsValid()
        {
            if (Supplair_Id.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void resettextfeilds()
        {
            Supplair_Id.Clear();
            Supplair_Name.Clear();
            Supplair_Contact_No.Clear();
            Supplair_Address.Clear();
            Supplair_Email.Clear();
        }

        private void supDetails_Load(object sender, EventArgs e)
        {
            Getsupdata();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }
    }
}
