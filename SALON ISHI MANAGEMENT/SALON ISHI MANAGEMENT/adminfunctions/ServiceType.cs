﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SALON_ISHI_MANAGEMENT.adminfunctions
{
    public partial class ServiceType : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        public ServiceType()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new dashboard().Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {
                   
                    SqlCommand cmd = new SqlCommand("INSERT into ServiceType VALUES (@servicetype)", con);
                    cmd.CommandType = CommandType.Text;

                

                    cmd.Parameters.AddWithValue("@servicetype", servicetypr.Text);
                  


                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    Getservicetypedata();
                    resettextfeilds();
                    


                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

      

        private bool IsValid()
        {
            if (servicetypr.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }
        private void resettextfeilds()
        {
            servicetypr.Clear();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ServiceType_Load(object sender, EventArgs e)
        {
            Getservicetypedata();
        }

        private void Getservicetypedata()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("select * from ServiceType",con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                ServiecTypeGridView.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void servicetypr_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
