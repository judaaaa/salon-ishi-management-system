﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT.adminfunctions
{
    public partial class ProductType : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");

        public ProductType()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("INSERT into ProductType VALUES (@Type)", con);
                    cmd.CommandType = CommandType.Text;



                    cmd.Parameters.AddWithValue("@Type", Type.Text);



                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    GetProductTypedata();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                catch (SqlException exception)
                {

                    MessageBox.Show("Product Type entered InValid.!!!  This Product Type is Already There!!" , "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }


            }
        }

        private void resettextfeilds()
        {
            Type.Clear();
        }

        private void GetProductTypedata()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("select * from ProductType", con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                GenderTypeGridView.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private bool IsValid()
        {
            if (Type.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            resettextfeilds();
        }

        private void ProductType_Load(object sender, EventArgs e)
        {
            GetProductTypedata();
        }
    }
}
