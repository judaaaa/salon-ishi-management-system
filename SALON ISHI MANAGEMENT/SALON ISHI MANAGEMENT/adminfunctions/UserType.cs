﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALON_ISHI_MANAGEMENT.adminfunctions
{
    public partial class UserType : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
        public UserType()
        {
            InitializeComponent();
        }

        private void servicetypr_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (IsValid())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("INSERT into UserType VALUES (@Type)", con);
                    cmd.CommandType = CommandType.Text;



                    cmd.Parameters.AddWithValue("@Type",Type.Text);



                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Data Submited Successfuly", "Saved", MessageBoxButtons.OK, MessageBoxIcon.None);

                    GetUsertypedata();
                    resettextfeilds();



                }
                catch (InvalidCastException exception)
                {

                    MessageBox.Show("" + exception, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void resettextfeilds()
        {
            Type.Clear();
        }

        private void GetUsertypedata()
        {
            try
            {
                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("select * from UserType", con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                UserTypeGridView.DataSource = dt;
            }
            catch (InvalidOperationException exc)
            {
                MessageBox.Show("" + exc, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool IsValid()
        {
            if (Type.Text == String.Empty)
            {
                MessageBox.Show("Data Requrid", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void UserType_Load(object sender, EventArgs e)
        {
            GetUsertypedata();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            new dashboard().Show();
            this.Hide();
        }

        private void UserTypeGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
