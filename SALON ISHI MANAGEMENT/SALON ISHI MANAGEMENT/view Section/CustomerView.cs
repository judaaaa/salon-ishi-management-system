﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SALON_ISHI_MANAGEMENT.view_Section
{
    public partial class CustomerView : Form
    {
        public CustomerView()
        {
            InitializeComponent();
        }

              private void CustomerView_Load(object sender, EventArgs e)
        {
            GetcustomerData();
        }

        private void GetcustomerData()
        {
            try {

                SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-35656H2;Initial Catalog=salonishi;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("Select * from CustomerD",con);
                DataTable dt = new DataTable();

                con.Open();

                SqlDataReader sdr = cmd.ExecuteReader();
                dt.Load(sdr);
                con.Close();

                customerDataGridView.DataSource = dt;

            } catch(Exception e) {

                MessageBox.Show(""+e ,"Error", MessageBoxButtons.OK,MessageBoxIcon.Error);

            }
        }

        private void customerDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
